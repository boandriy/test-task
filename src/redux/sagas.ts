import { call, put, takeEvery } from "redux-saga/effects";
import {
  getCountries,
  getAllCountries,
  getFilterCountries
} from "../Pages/mainPage/api";
import {
  FETCHING_FILTER_COUNTRIES,
  FETCHING_ALL_COUNTRIES,
  FETCHING_COUNTRIES,
  FETCH_FILTER_ERROR,
  FETCH_FILTER_SUCCESSES,
  FETCH_ALL_ERROR,
  FETCH_ALL_SUCCESSES,
  FETCH_ERROR,
  FETCH_SUCCESSES
} from "./actionTypes";

interface Props {
  payload: any;
  id: number;
  page?: number;
  query?: number;
  number?: number;
}

function* getCountriesWorker(props: Props) {
  const { id } = props;
  try {
    const countries = yield call(getCountries, id);
    yield put({ type: FETCH_SUCCESSES, payload: countries });
  } catch (error) {
    yield put({ type: FETCH_ERROR, payload: error });
  }
}

function* getAllCountriesWorker(props: Props) {
  const {
    payload: { id, page }
  } = props;
  try {
    const moreCountries = yield call(getAllCountries, id, page);
    yield put({ type: FETCH_ALL_SUCCESSES, payload: moreCountries });
  } catch (error) {
    yield put({ type: FETCH_ALL_ERROR, payload: error });
  }
}

function* getFilterCountriesWorker(props: Props) {
  const {
    payload: { id, number, query }
  } = props;
  try {
    const filterCountries = yield call(getFilterCountries, id, number, query);
    yield put({ type: FETCH_FILTER_SUCCESSES, payload: filterCountries });
  } catch (error) {
    yield put({ type: FETCH_FILTER_ERROR, payload: error });
  }
}

function* watcherCountries() {
  yield takeEvery(FETCHING_COUNTRIES, getCountriesWorker);
  yield takeEvery(FETCHING_ALL_COUNTRIES, getAllCountriesWorker);
  yield takeEvery(FETCHING_FILTER_COUNTRIES, getFilterCountriesWorker);
}

export default watcherCountries;

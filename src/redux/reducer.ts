const initialState = {
  loading: false,
  countries: [],
  errorMessage: ""
};

export default (state = initialState, action: any) => {
  switch (action.type) {
    case "FETCHING_COUNTRIES":
      return {
        ...state,
        loading: true
      };
    case "FETCH_SUCCESSES":
      return {
        ...state,
        loading: false,
        countries: action.payload
      };
    case "FETCH_ERROR":
      return {
        ...state,
        loading: false,
        countries: [],
        errorMessage: action.payload
      };
    case "FETCHING_ALL_COUNTRIES":
      return {
        ...state,
        loading: true
      };
    case "FETCH_ALL_SUCCESSES":
      return {
        ...state,
        loading: false,
        countries: [...state.countries, ...action.payload]
      };
    case "FETCH_ALL_ERROR":
      return {
        ...state,
        loading: false,
        countries: [],
        errorMessage: action.payload
      };

      case "FETCHING_FILTER_COUNTRIES":
        return {
          ...state,
          loading: true
        };
      case "FETCH_FILTER_SUCCESSES":
        return {
          ...state,
          loading: false,
          countries: action.payload
        };
      case "FETCH_FILTER_ERROR":
        return {
          ...state,
          loading: false,
          countries: [],
          errorMessage: action.payload
        };

    case "CLEAR_COUNTRIES_ARRAY":
      return {
        ...state,
        countries: []
      };
    default:
      return state;
  }
};

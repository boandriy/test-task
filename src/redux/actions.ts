import { CLEAR_COUNTRIES_ARRAY } from "./actionTypes";

export const clearCountries = () => ({
  type: CLEAR_COUNTRIES_ARRAY  
})
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

import reducer from "./reducer";
import saga from "./sagas";

const rootSaga = createSagaMiddleware(saga);

const store = createStore(
  reducer,
  applyMiddleware(rootSaga)
);

rootSaga.run(saga);

export default store;
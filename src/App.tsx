import React from 'react';
import './App.scss';
import './styles/mainPage/test.scss';
import MainPage from "./Pages/mainPage";

function App() {
  return (
    <MainPage />
  );
}

export default App;

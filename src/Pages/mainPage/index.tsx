import React from 'react';
import ButtonToolbar from "react-bootstrap/ButtonToolbar";
import Button from 'react-bootstrap/Button'
import Modal from './components/organisms/InfoModal';
import {Contacts} from './api';

const USCountryID = 226;

type Props = {
    test?: boolean
    test2?:string
};

type State = {
    isModalAOpened: boolean;
    isModalBOpened: boolean;
    isModalCOpened: boolean;
    item: Contacts;
};

export default class MainPage extends React.Component<Props, State> {
    constructor(props:Props) {
        super(props);
        this.state = {
            isModalAOpened: false,
            isModalBOpened: false,
            isModalCOpened: false,
            item: {} as Contacts,
        };
    }
    toggleModalA = () => {
        this.setState({
            isModalAOpened: !this.state.isModalAOpened
        })
    };

    toggleModalC = () => {
        this.setState({
            isModalCOpened: !this.state.isModalCOpened
        })
    };

    toggleModalB = () => {
        this.setState({
            isModalBOpened: !this.state.isModalBOpened
        })
    };

    setItem = (item: Contacts) => {
        this.setState({
            item
        }, () => {
            this.toggleModalC()
        })
    };

    render(){
        return (
            <div className="page-container">
                <ButtonToolbar className="justify-content-around">
                    <Button className='buttonA' onClick={this.toggleModalA}>Button A</Button>
                    <Button className='buttonB' onClick={this.toggleModalB}>Button B</Button>
                </ButtonToolbar>
                <Modal
                    show={this.state.isModalAOpened}
                    toggle={() => {this.toggleModalA()}}
                    title='Modal A'
                    toggleAButton={() => {}}
                    toggleBButton={() => {
                        this.toggleModalA();
                        this.toggleModalB()
                    }}
                    toggleCModal={() => {
                        this.toggleModalA();
                    }}
                    setItem = {this.setItem}
                    contacts={true}
                    isDescription={false}
                />
                <Modal
                    show={this.state.isModalBOpened}
                    toggle={() => {this.toggleModalB()}}
                    title='Modal B'
                    toggleAButton={() => {
                        this.toggleModalA();
                        this.toggleModalB()
                    }}
                    toggleCModal={() => {
                        this.toggleModalB();
                    }}
                    setItem = {this.setItem}
                    toggleBButton={() => {}}
                    contacts={true}
                    contryId={USCountryID}
                    isDescription={false}
                />
                <Modal
                    show={this.state.isModalCOpened}
                    toggle={() => {this.toggleModalC()}}
                    title='Modal C'
                    toggleAButton={() => {
                        this.toggleModalC();
                        this.toggleModalA();
                    }}
                    toggleBButton={() => {
                        this.toggleModalC();
                        this.toggleModalB();
                    }}
                    contacts={false}
                    isDescription={true}
                    item = {this.state.item}
                />
            </div>
        )
    }
}

import axios, { AxiosResponse } from "axios";

const TOKEN =
  "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxNzEiLCJleHAiOjE2MDM3ODM0Mzd9.3ievseHtX0t3roGh7nBuNsiaQeSjfiHWyyx_5GlOLXk";
export interface requestResponse {
  total: string;
  contacts_ids: number[];
  contacts: any;
}
export interface Contacts {
  id: number;
  first_name?: string;
  last_name?: string;
  email?: string;
  phone_number?: string;
  country_id?: number;
  comment?: string;
  status?: number;
  favorite?: number;
  color?: string;
  is_ccb?: number;
  is_pco?: number;
  is_mc?: number;
  is_elvanto?: number;
  is_breeze?: number;
  is_duplicate?: number;
  has_duplicate?: number;
  is_ccb_primary_contact?: number;
  is_pco_primary_contact?: number;
  is_mc_primary_contact?: number;
  is_elvanto_primary_contact?: number;
  is_breeze_primary_contact?: number;
  unsub_by_user_id?: number;
  is_contact_deleted?: number;
  is_contact_mark_deleted?: number;
  country?: any;
  number_lookup?: number;
  groups?: any;
  contacts_ids?: number[];
  primary_contact_id?: number;
  primary_contact_ids?: number[];
  primary_contact_ids_by_app?: any;
  multiple_primary_contacts?: number;
  is_primary?: boolean;
  app_contact_ids?: number[];
}

interface response {
  data: requestResponse;
  status: number;
  statusText: string;
  headers: any;
  config: any;
  request: any;
}

export function getContactsUsingGET(
  countryParam?: number,
  page?: number,
  query?: string
): Promise<requestResponse> {
  const headers = { Authorization: TOKEN };
  const params = {
    companyId: 171,
    page: page || 1,
    countryId: countryParam,
    query: query
  };
  return axios
    .get(`https://api.dev.pastorsline.com/api/contacts.json`, {
      headers: headers,
      params: params
    })
    .then((res: AxiosResponse<response>) => {
      return res.data;
    });
}

export const getCountries = async (id: number) => {
  const response = await getContactsUsingGET(id);
  const responseContacts = response.contacts;
  const contacts = Object.values(responseContacts);
  return contacts;
};

export const getAllCountries = async (id: number, page: number) => {
  const largeResponse = await getContactsUsingGET(id, page);
  const responseNewContacts = largeResponse.contacts;
  const moreContacts = Object.values(responseNewContacts);
  return moreContacts;
};

export const getFilterCountries = async (
  id: number,
  number: number,
  query: any
) => {
  const filterResponse = await getContactsUsingGET(id, number, query);
  const filterResponseContacts = filterResponse.contacts;
  let filterContacts = [];
  for (let key in filterResponseContacts) {
    filterContacts.push(filterResponseContacts[key]);
  }
  return filterContacts;
};

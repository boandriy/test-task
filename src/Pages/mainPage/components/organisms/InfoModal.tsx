import React, { ChangeEvent } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import ListGroup from "react-bootstrap/ListGroup";
import ButtonToolbar from "react-bootstrap/ButtonToolbar";
import { Contacts } from "../../api";
import InfiniteScroll from "react-infinite-scroller";
import UserDetailsList from "../atoms/UserDetailsList";
import UserDetailsItem from "../atoms/UserDetailsItem";
import { debounce } from "lodash";

import { connect } from "react-redux";
import { clearCountries } from "../../../../redux/actions";
import { FETCHING_COUNTRIES, FETCHING_FILTER_COUNTRIES, FETCHING_ALL_COUNTRIES } from "../../../../redux/actionTypes";

interface Props {
  show: boolean;
  toggle: () => void;
  title: string;
  toggleAButton: () => void;
  toggleBButton: () => void;
  contacts: boolean;
  isDescription: boolean;
  toggleCModal?: () => void;
  item?: Contacts;
  contryId?: number;
  setItem?: (item: Contacts) => void;
  getCountries?: (contryId: number | undefined) => void;
  getAllCountries?: (contryId: number, page: number) => void;
  countries: Contacts[] | undefined;
  clearCountries: (arg0: any) => any;
}
interface State {
  evenOnly: boolean;
  filterValue: string;
}

class InfoModal extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      evenOnly: false,
      filterValue: ""
    };
  }

  getInitialUsers = () => this.props.getCountries && this.props.getCountries(this.props.contryId);

  getMoreContacts = (page: number) => this.props.getAllCountries(this.props.contryId, page);

  getFilteredContacts = debounce(async (query?: string) => {
    this.props.getFilterCountries(this.props.contryId, 1, query)
  }, 500);

  componentDidUpdate(
    prevProps: Readonly<Props>,
    prevState: Readonly<State>,
    snapshot?: any
  ): void {
    const { clearCountries } = this.props;
    if (prevProps.show && !this.props.show) {
      clearCountries([]);
      this.setState({filterValue: ''})
    } else if (!prevProps.show && this.props.show) {
      this.getInitialUsers();
    }
  }

  handleEvenCheck = () => {
    this.setState({
      evenOnly: !this.state.evenOnly
    });
  };

  handleFilter = (event: ChangeEvent<HTMLInputElement>) => {
    this.setState({ filterValue: event.target.value }, () => {
      this.getFilteredContacts(this.state.filterValue);
    });
  };

  render() {
    const { countries } = this.props;
    return (
      <Modal
        show={this.props.show}
        onHide={this.props.toggle}
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
        className="main-modal"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            {this.props.title}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ButtonToolbar className="justify-content-around">
            <Button className="buttonA" onClick={this.props.toggleAButton}>
              All Contacts
            </Button>
            <Button className="buttonB" onClick={this.props.toggleBButton}>
              US Contacts
            </Button>
            <Button className="buttonC" onClick={this.props.toggle}>
              Close
            </Button>
          </ButtonToolbar>
          {this.props.contacts ? (
            <>
              <form action="" className="filter-form">
                <label>Filter</label>
                <input
                  placeholder="Type your desired filter.."
                  onChange={this.handleFilter}
                  type="text"
                  value={this.state.filterValue}
                />
              </form>
              <ListGroup>
                <InfiniteScroll
                  pageStart={1}
                  loadMore={this.getMoreContacts}
                  hasMore={true}
                  initialLoad={false}
                  loader={
                    <div className="loader" key={0}>
                      Loading ...
                    </div>
                  }
                  useWindow={false}
                >
                  {countries &&
                    countries.map(contact => (
                      <>
                        {this.state.evenOnly && contact.id % 2 === 0 && (
                          <UserDetailsItem
                            item={contact}
                            key={contact.id}
                            handleClick={() => {
                              this.props.setItem && this.props.setItem(contact);
                              this.props.toggleCModal &&
                                this.props.toggleCModal();
                            }}
                          />
                        )}
                        {!this.state.evenOnly && (
                          <UserDetailsItem
                            item={contact}
                            key={contact.id}
                            handleClick={() => {
                              this.props.setItem && this.props.setItem(contact);
                              this.props.toggleCModal &&
                                this.props.toggleCModal();
                            }}
                          />
                        )}
                      </>
                    ))}
                </InfiniteScroll>
              </ListGroup>{" "}
            </>
          ) : null}
          {this.props.item ? <UserDetailsList item={this.props.item} /> : null}
        </Modal.Body>
        {this.props.contacts && (
          <Modal.Footer>
            <span>Even id's only</span>
            <input
              type={"checkbox"}
              onChange={this.handleEvenCheck}
              checked={this.state.evenOnly}
            />
          </Modal.Footer>
        )}
      </Modal>
    );
  }
}

const mapStateToProps = (state: any) => {
  if (state) {
    return {
      countries: state.countries
    };
  }
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    getCountries: (id: number) => dispatch({ type: FETCHING_COUNTRIES, id }),
    getAllCountries: (id: number, page: number) =>
      dispatch({
        type: FETCHING_ALL_COUNTRIES,
        payload: { id: id, page: page }
      }),
    getFilterCountries: (id: number, number: number, query: any) =>
      dispatch({
        type: FETCHING_FILTER_COUNTRIES,
        payload: { id: id, number: number, query: query }
      }),
    clearCountries: () => dispatch(clearCountries())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InfoModal);

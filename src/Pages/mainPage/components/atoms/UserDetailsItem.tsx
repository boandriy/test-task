import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup'
import {Contacts} from "../../api";


interface Props {
    item:Contacts;
    handleClick: () => void;
}


export default function(props: Props){
    return (
        <ListGroup.Item key={props.item.id} onClick={props.handleClick}>
            <span>{props.item.id}</span>
            <span>{props.item.first_name ? props.item.first_name : 'No first Name'}</span>
            <span>{props.item.phone_number ? props.item.phone_number : 'No nymber'}</span>
        </ListGroup.Item>
    )
}

import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup'
import {Contacts} from "../../api";

interface Props {
    item:Contacts
}

export default function(props: Props){
    return (
        <ListGroup>
            <ListGroup.Item><span>id</span> <span>{props.item.id}</span></ListGroup.Item>
            <ListGroup.Item><span>first name</span> <span>{props.item.first_name}</span></ListGroup.Item>
            <ListGroup.Item><span>last name</span> <span>{props.item.last_name}</span></ListGroup.Item>
            <ListGroup.Item><span>phone number</span> <span>{props.item.phone_number}</span></ListGroup.Item>
        </ListGroup>
    )
}
